package net.proselyte.jdbctutorial.dao;

import net.proselyte.jdbctutorial.model.Developer;

public interface DeveloperDAO extends GenericDAO<Developer, Long> {
}
