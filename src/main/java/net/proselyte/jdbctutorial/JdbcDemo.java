package net.proselyte.jdbctutorial;


import net.proselyte.jdbctutorial.dao.DeveloperDAO;
import net.proselyte.jdbctutorial.dao.jdbc.JdbcDeveloperDAOImpl;
import net.proselyte.jdbctutorial.model.Developer;

import java.sql.SQLException;

public class JdbcDemo {
    public static void main(String[] args) throws SQLException {
        DeveloperDAO developerDAO = new JdbcDeveloperDAOImpl();
        Developer developer = developerDAO.getById(2L);

        System.out.println(developer);
    }
}
